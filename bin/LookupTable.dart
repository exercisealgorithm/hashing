
import 'hashing.dart';

extension GetByKeyIndex on Map {
  elementAt(int index) => this.values.elementAt(index);
}

class LookupTable{
  static int size = 11;
  late var arrayX = new List.filled(size, -1);
  late Hashing mapY;  //เอาไว้เก็บข้อมูล Key และ Value , เผื่อเรียกดูข้อมูล Value ใน Key

  
  LookupTable(Hashing x){
    mapY = x;
    for(int i=0;i<mapY.newMap.length;i++){
      arrayX[mapY.newMap.keys.elementAt(i)]=mapY.newMap.keys.elementAt(i);
    }
  }
  
  int getSize(){
    return size;
  }
  //get(k)
  dynamic get(var key){ 
    int x = key%size;                     // % ก่อน เมื่อให้ ค่า key มีค่าตรงกัน
    for(int i=0;i<arrayX.length;i++){
      if(arrayX[i]==x){
        return mapY.newMap[x];
      }
    }
    return null;
  }

  put(var key,var val){
    var x = key%size;                     // % ก่อน เมื่อให้ ค่า key มีค่าตรงกัน
    for(int i=0; i<arrayX.length;i++){
      if(arrayX[i]==x){
        mapY.newMap[x] = val;
      }else{
        mapY.newMap[x]= val;                 //เก็บข้อมูล val ลงในไปใน mapY.newMap
        arrayX[x] = x;                       //เพิ่ม key ลงไปใน List
      }
    }
  }

  dynamic remove(var key){
    var x = key%size;                     // % ก่อน เมื่อให้ ค่า key มีค่าตรงกัน
   for(int i=0; i<arrayX.length;i++){
      if(arrayX[i]==x){
        arrayX[x] = -1;
        mapY.newMap.remove(x); 
        return x;                         //return Key;
      }
    }
    return null;
  }

  @override
  String toString() {
    return arrayX.toString();
  }
}