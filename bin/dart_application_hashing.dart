import 'LookupTable.dart';
import 'hashing.dart';
import 'dart:io';


void main() {
  Hashing x = new Hashing(); //สร้างข้อมูลที่มี Key เป็นตัวเลข
  x.addKeyVal(206, 'Adsadawut');
  x.addKeyVal(108, 'Arthit');
  x.addKeyVal(523, 'Thunwa');
  x.addKeyVal(429, 'Grit');
  //  x[206]= {'Adsadawut'};
  //  x[108]= {'Arthit'};
  //  x[523]= {'Thunwa'};
  //  x[429]= {"Grit"};
  x.process();
  print(x);
  LookupTable table = new LookupTable(x);
  
  //Test get(k)
  print(table);
  print("Test get(k)");
  stdout.write("get(206): ");
  print(table.get(206));
  stdout.write("get(208): ");
  print(table.get(208));

  //Test put(k,v)
  print("Test put(k,v)");
  stdout.write("put(206,'KongKang'): ");
  table.put(206,"KongKang");
  print(table);
  stdout.write("Key of 206 is ");
  print(table.arrayX[206%table.getSize()]);
  stdout.write("Now the value of 206 is ");
  print(table.get(206));
  print("");

  stdout.write("put(205,'sdsdsd'): ");
  table.put(205,"sdsdsd");
  print(table);
  stdout.write("Key of 205 is ");
  print(table.arrayX[205%table.getSize()]);
  stdout.write("Now the value of 205 is ");
  print(table.get(205));

  //Test remove(k,v)
  print("Test remove(k)");
  stdout.write("remove(208): ");  // 208 ไม่ตรงกับค่า key ใดๆ
  print(table.remove(208));
  print(table);
  print(table.mapY);
  
  print("");
  stdout.write("remove(206): ");  
  print(table.remove(206));
  print(table);
  print(table.mapY);


//  เมทอดที่นำไว้หา index ของ Key ใน table.arrayX
 int? findIndexOfKey(var key){          // % ก่อน เมื่อให้ ค่า key มีค่าตรงกัน
  var x =key%table.getSize();
  for(int i=0; i<table.arrayX.length;i++){
    if(x==table.arrayX.elementAt(i)){  //ถ้าค่า key ที่แปลงค่าแล้ว มีค่าเท่ากับ ค่า key ที่ LookupTable ตัวที่ i เก็บไว้
      return i;
    }
  }
  return null;
 }
  stdout.write("Index เมื่อ key = 429 :");
  print(findIndexOfKey(429));
  stdout.write("Index เมื่อ key = 206 :");  //ถูก remove ไปก่อนหน้าแล้ว
  print(findIndexOfKey(206));
  stdout.write("Index เมื่อ key = 530 :");
  print(findIndexOfKey(530));
}