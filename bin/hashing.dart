import 'LookupTable.dart';
extension GetByKeyIndex on Map {
  elementAt(int index) => this.values.elementAt(index);
}

class Hashing{
  var element;
  var key;
  var val;
  late Map mapX;
  late Map newMap;         //เป็นตัวเก็บค่า Key และ Value ที่ถูกแปลงค่า Key แล้ว
  
  Hashing(){
    mapX = new Map();
    newMap = new Map();
  }

  addKeyVal(var key,var val){
    this.key=key;
    this.val=val;
    mapX[key]=val;
  }
  
  Map process(){                                     
    for(int i=0;i < mapX.length;i++){
      element =mapX.keys.elementAt(i)%LookupTable.size;            //11 คือ size ที่เป็นเลขขนาดของ List ใน LookupTable
      newMap[element] = mapX.values.elementAt(i);
    }
    return newMap;
  }

  

  @override
  String toString() {
    return newMap.toString();
  }
}